resource "aws_security_group" "main_security_group" {
  name        = var.security_group_name
  description = "Security Group ${var.security_group_name}"
  vpc_id      = var.vpc_id
  tags        = merge(var.tags, map("Name", format("%s", var.security_group_name)))

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group_rule" "ingress" {
  count             = length(var.allow_ports)
  type              = ingress
  from_port         = element(var.allow_ports, count.index)
  to_port           = element(var.allow_ports, count.index)
  protocol          = "tcp"
  cidr_blocks       = [var.source_cidr_blocks]
  security_group_id = aws_security_group.main_security_group.id
}

resource "aws_security_group_rule" "egress" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = -1
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.main_security_group.id
}
