variable "security_group_name" {
  description = "The name for the security group."
  type        = string
}

variable "source_cidr_blocks" {
  description = "A list of the source CIDR blocks."
  type        = list
}

variable "tags" {
  description = "A map of the tags to apply to the SG."
  type        = map
}

variable "vpc_id" {
  description = "The ID of the target VPC"
}

variable "allow_ports" {
  description = "A list of ports to allow"
  type        = list
}
